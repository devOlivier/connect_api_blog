import React, { Component } from 'react'

import PostComplet from "./PostComplet/PostComplet"
import NvPost from './NvPost/NvPost'
import Post from './Post/Post'
import './Blog.css'
import axios from 'axios';


class Blog extends Component {

    state = {
        posts : [],
        selectPostId : null
    }

    componentDidMount () {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(response => {
                const nbData = response.data.slice(0,9)
                const nameAuthor = nbData.map(post => {
                    return {
                        ...post,
                        author : 'Olivier',
                    }
                })
                this.setState({posts : nameAuthor}) 
                console.log('componentDidMount : ' ,   response)
            })
    }

    clickArticle = (id) => {
        console.log(id);
        this.setState({selectPostId : id})
    }

    render () {

        const data = this.state.posts.map(post => {
            return <Post 
            key={post.id}
            titre={post.title} 
            author={post.author}
            selectedArticle={() => this.clickArticle(post.id)}    
            />
        })

        return (
            <div>
                <section>
                    <NvPost />
                </section>
                <section>
                    <PostComplet id={this.state.selectPostId}/>
                </section>
                <h2 className="text-center my-5">Tous les Articles ...</h2>
                <section className="Posts">
                {data}
                </section>

            </div>
        );
    }
}

export default Blog;
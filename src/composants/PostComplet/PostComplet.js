import React, { Component } from 'react'
import './PostComplet.css'
import axios from 'axios'

class PostComplet extends Component {

   state = {
       loadingPost : null
   }

   componentDidUpdate () {
       if (this.props.id) {
           //Si pas encore de post affiché
           if(!this.state.loadingPost || (this.state.loadingPost && this.state.loadingPost.id !== this.props.id)) {
               axios.get('https://jsonplaceholder.typicode.com/posts/' + this.props.id)
                   .then(response => {
                       console.log(response)
                       this.setState({ loadingPost: response.data })
                   })
           }
       }
   }

    render () {
        let post = <p className="text-center">Choisis ton article pour le lire en entier</p>;
        let upload = <p className="text-center">Chargement...</p> 
            if (this.props.id) {
                console.log('upload :' , upload)
            }
            if (this.state.loadingPost) {
                post = (
                    <div className="PostComplet">
                        <h1> {this.state.loadingPost.title} </h1>
                        <p> {this.state.loadingPost.body} </p>
                        <p> Article numéro {this.props.id} </p>
                        <div className="Edit">
                            <button className="btn btn-danger my-3">Supprimer</button>
                        </div>
                    </div>

                );
            }
        return post;
    }
}

export default PostComplet;
import React, { Component } from 'react'
import './NvPost.css'
import axios from 'axios'

class NvPost extends Component {

    state = {
        title: '',
        content: '',
        author: 'Olivier'
    }

    addPost = () => {
        const contentPost = {
            title : this.state.title,
            content : this.state.content,
            author : this.state.author,
        }
        axios.post('https://jsonplaceholder.typicode.com/posts' , contentPost)
            .then(response => (
                console.log(response)
            ))
    }

    render () {
        return (
            <div className="NvPost form-group mt-5">
                <h1 className="mt-5">Ecrire un nouveau Post</h1>
                <input className="form-control mt-3" placeholder="Titre du post" type="text" value={this.state.title} onChange={(e) => this.setState({title: e.target.value})} />
                <textarea className="form-control mt-3" placeholder="Sujet du post" rows="4" value={this.state.content} onChange={(e) => this.setState({content: e.target.value})} />
                <select className="form-control mt-3" value= {this.state.author} onChange={(e) => this.setState({author: e.target.value})}>
                    <option value="Hugo">Hugo</option>
                    <option value="Juliette">Juliette</option>
                    <option value="John">John</option>
                </select>
                <button 
                    onClick={this.addPost}
                    className="btn btn-primary my-3">
                    Ajouter un Article
                </button>
            </div>
        );
    }
}
export default NvPost;